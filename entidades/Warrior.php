<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Warrior
 *
 * @author pabhoz
 */
class Warrior extends Character{
    
    protected $clase = "warrior";
    
    public function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag) {
        parent::__construct($nombre, $raza, $hp*2, $mn*0, $str*1.5, $md, $ag);
    }
    
    public function attack(\ICharacter $character) {
        $dmg = (get_class($character) != "Rogue") ? $this->getStr() * 0.6 : 
            $this->getStr() * 2.5 ;
        echo $this->getNombre()." ataca a ".$character->getNombre().", causando "
                . "$dmg de daño.";
        $character->getHurt($dmg);        
    }
    
    public function dramaticDeath() {
        echo "Yo ".$this->getNombre().", el gran guerrero, he sido derrotado en "
                . "batalla.";
    }
}
