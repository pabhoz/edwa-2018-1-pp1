<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rogue
 *
 * @author pabhoz
 */
class Rogue extends Character{
    
    protected $clase = "rogue";
    
    public function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag) {
        parent::__construct($nombre, $raza, $hp*1.2, $mn*0, $str, $md, $ag*2);
    }
    
    public function attack(\ICharacter $character) {
        $this->magicAttack($character);
    }
    
    public function magicAttack(\ICharacter $character) {
        
        $dmg = 0;
        
        if(get_class($character) == "Mage"){
            $dmg = $this->getStr() * 3.5 ;
        }elseif (get_class($character) == "Warrior") {
            $dmg = $this->getStr() * 0.5 ;
        }else{
            $dmg = $this->getStr() * 1.5; 
        }
        
        echo $this->getNombre()." ataca a ".$character->getNombre().", causando "
                . "$dmg de daño.";
        $character->getHurt($dmg);
    }
    
    public function dramaticDeath() {
        echo $this->getNombre()." muere en silencio.";
    }
}
