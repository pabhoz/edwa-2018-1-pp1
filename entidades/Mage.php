<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MAge
 *
 * @author pabhoz
 */
class Mage extends Character{
    
    protected $clase = "mage";
    
    public function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag) {
        parent::__construct($nombre, $raza, $hp, $mn*5, $str, $md*2.5, $ag);
    }
    
    public function attack(\ICharacter $character) {
        $this->magicAttack($character);
    }
    
    public function magicAttack(\ICharacter $character) {
        $dmg = (get_class($character) != "Warrior") ? $this->getStr() * 1.5 : 
            $this->getStr() * 4.5 ;
        echo $this->getNombre()." ataca a ".$character->getNombre().", causando "
                . "$dmg de daño.";
        $character->getHurt($dmg);
    }
    
    public function dramaticDeath() {
        echo "Oh! Yo ".$this->getNombre().", he sido derrotado en "
                . "batalla.";
    }
}