<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Character
 *
 * @author pabhoz
 */
class Character extends CharacterFactory{

    private $nombre;
    private $raza;
    private $hp;
    private $mn;
    private $str;
    private $md;
    private $ag;

    function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag) {
        $this->nombre = $nombre;
        $this->raza = $raza;
        $this->hp = $hp;
        $this->mn = $mn;
        $this->str = $str;
        $this->md = $md;
        $this->ag = $ag;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getRaza() {
        return $this->raza;
    }

    public function getClase() {
        return $this->clase;
    }

    public function getHp() {
        return $this->hp;
    }

    public function getMn() {
        return $this->mn;
    }

    public function getStr() {
        return $this->str;
    }

    public function getMd() {
        return $this->md;
    }

    public function getAg() {
        return $this->ag;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setRaza($raza) {
        $this->raza = $raza;
    }

    public function setClase($clase) {
        $this->clase = $clase;
    }

    public function setHp($hp) {
        $this->hp = $hp;
    }

    public function setMn($mn) {
        $this->mn = $mn;
    }

    public function setStr($str) {
        $this->str = $str;
    }

    public function setMd($md) {
        $this->md = $md;
    }

    public function setAg($ag) {
        $this->ag = $ag;
    }

    public function attack(\ICharacter $character) {
        $baseStat = ($this->getClase() == "normal") ? max($this->str, $this->md, $this->ag) : null;
        $dmg = $baseStat * 0.4;
        $character->getHurt($dmg);
        echo $this->getNombre()." ataca a ".$character->getNombre().", causando $dmg de daño.";
    }

    public function dramaticDeath() {
        echo "Arrrg, yo ".$this->getNombre()." he sido derrotado en batalla.";
    }

}
