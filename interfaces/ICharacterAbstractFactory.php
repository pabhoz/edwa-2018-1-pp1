<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pabhoz
 */
interface ICharacterAbstractFactory {
    
    public function getMage($nombre, $raza, $hp, $mn, $str, $md, $ag):Mage;
    public function getRogue($nombre, $raza, $hp, $mn, $str, $md, $ag):Rogue;
    public function getWarrior($hp, $mn, $str, $md, $ag, $nombre = "Warriorcillo", $raza = "humano"):Warrior;
    
}
