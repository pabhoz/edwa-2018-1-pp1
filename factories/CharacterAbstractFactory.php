<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CharacterAbstractFactory
 *
 * @author pabhoz
 */
class CharacterAbstractFactory implements ICharacterAbstractFactory{
    //put your code here
    public function getMage($nombre, $raza, $hp, $mn, $str, $md, $ag): \Mage {
        return new Mage($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }

    public function getRogue($nombre, $raza, $hp, $mn, $str, $md, $ag): \Rogue {
       return new Rogue($nombre, $raza, $hp, $mn, $str, $md, $ag); 
    }

    public function getWarrior($hp, $mn, $str, $md, $ag, $nombre = "Warriorcillo", $raza = "humano"): \Warrior {
        return new Warrior($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }

}
