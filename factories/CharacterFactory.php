<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CharacterFactory
 *
 * @author pabhoz
 */
abstract class CharacterFactory implements ICharacter{
    
    protected $clase = "normal";
    
    abstract function attack(\ICharacter $character);
    
    public function getHurt($dmg) {
        $this->setHp($this->getHp()-$dmg);
        if($this->getHp()<= 0){  $this->dramaticDeath(); }
    }
    
    abstract function dramaticDeath();
}
