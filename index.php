<?php

spl_autoload_register(function($class){
   if(file_exists("entidades/".$class.".php")){
       include "entidades/".$class.".php";
   }
   if(file_exists("interfaces/".$class.".php")){
       include "interfaces/".$class.".php";
   }
   if(file_exists("factories/".$class.".php")){
       include "factories/".$class.".php";
   }
});

$factory = new CharacterAbstractFactory();

//Create a warrior
$warrior = $factory->getWarrior(100, 50, 50, 0, 25);
//Create a Mage
$mage = $factory->getMage("mage", "elfo", 100, 50, 10, 50, 15);
//Create a Rogue
$rogue = $factory->getRogue("rogue", "hombre lobo", 100, 10, 20, 3, 50);
// The Warrior fight the mage
$warrior->attack($mage); echo "\n";
// The Mage fight the Rogue
$mage->attack($rogue); echo "\n";
// The Rogue fight the Warrior
$rogue->attack($warrior); echo "\n";